package com.afs.tdd;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;
import java.util.Queue;

public class MarsRover {
    private final Location location;

    private final Deque<MarsRoverCommand> marsRoverCommandQueue;

    public MarsRover(Location location) {
        this.location = location;
        this.marsRoverCommandQueue = new ArrayDeque<>();
    }

    public void addCommand(MarsRoverCommand marsRoverCommand) {
        marsRoverCommandQueue.add(marsRoverCommand);
    }

    public void executeCommands() {
        for (MarsRoverCommand command : marsRoverCommandQueue) {
            command.execute();
        }
    }

    public void executeCommand(Command command) {
        switch (command) {
            case MoveForward:
                moveForward();
                break;
            case TurnLeft:
                turnLeft();
                break;
            case TurnRight:
                turnRight();
                break;
            case MoveBackward:
                moveBackward();
                break;
        }
    }

    public Location getLocation() {
        return location;
    }

    public void moveForward() {
        switch (location.getDirection()) {
            case North:
                location.setLocationY(location.getLocationY() + 1);
                break;
            case South:
                location.setLocationY(location.getLocationY() - 1);
                break;
            case East:
                location.setLocationX(location.getLocationX() + 1);
                break;
            case West:
                location.setLocationX(location.getLocationX() - 1);
                break;
        }
    }

    public void moveBackward() {
        switch (location.getDirection()) {
            case North:
                location.setLocationY(location.getLocationY() - 1);
                break;
            case South:
                location.setLocationY(location.getLocationY() + 1);
                break;
            case East:
                location.setLocationX(location.getLocationX() - 1);
                break;
            case West:
                location.setLocationX(location.getLocationX() + 1);
                break;
        }
    }

    public void turnLeft() {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.West);
                break;
            case South:
                location.setDirection(Direction.East);
                break;
            case East:
                location.setDirection(Direction.North);
                break;
            case West:
                location.setDirection(Direction.South);
                break;
        }
    }

    public void turnRight() {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.East);
                break;
            case South:
                location.setDirection(Direction.West);
                break;
            case East:
                location.setDirection(Direction.South);
                break;
            case West:
                location.setDirection(Direction.North);
                break;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarsRover marsRover = (MarsRover) o;
        return Objects.equals(location, marsRover.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location);
    }
}
