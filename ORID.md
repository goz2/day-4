## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective): Today, i learned and practiced Test-Driven Development (TDD). i engaged in activities related to TDD, such as writing tests before writing code and following the red-green-refactor cycle. I focused on understanding the principles and benefits of TDD and applying them in practical coding exercises.

- R (Reflective): Excited.

- I (Interpretive): Today's class was quite engaging, and I found the concept of TDD to be highly valuable. By writing tests before implementing the code, TDD promotes a disciplined approach to software development and ensures that the code meets the desired requirements. It also helps in identifying and fixing bugs early in the development process.

- D (Decisional): I would like to apply what I have learned today in my future software development projects. I will strive to incorporate TDD as a regular practice in my coding workflow. By writing tests upfront and using TDD principles, I aim to enhance the reliability and robustness of my code while reducing the chances of introducing errors. 
